﻿public enum EzEaseType {
    Linear,

    SineIn,
    SineOut,
    SineInOut,

    BackIn,
    BackOut,
    BackInOut,

    BounceIn,
    BounceOut,
    BounceInOut,

    ElasticIn,
    ElasticOut,
    ElasticInOut,
    Punch,

    CircIn,
    CircOut,
    CircInOut,

    CubicIn,
    CubicOut,
    CubicInOut,

    ExpoIn,
    ExpoOut,
    ExpoInOut,

    QuadIn,
    QuadOut,
    QuadInOut,

    QuartIn,
    QuartOut,
    QuartInOut,

    QuintIn,
    QuintOut,
    QuintInOut,
}

public static class EzEaseTypeExtensions {
    public static System.Func<float, float, float, float, float> GetFunction(this EzEaseType easeType) {
        switch (easeType) {
            case EzEaseType.Linear:         return EzEaseLinear.None;

            case EzEaseType.SineIn:         return EzEaseSinusoidal.In;
            case EzEaseType.SineOut:        return EzEaseSinusoidal.Out;
            case EzEaseType.SineInOut:      return EzEaseSinusoidal.InOut;

            case EzEaseType.BackIn:         return EzEaseBack.In;
            case EzEaseType.BackOut:        return EzEaseBack.Out;
            case EzEaseType.BackInOut:      return EzEaseBack.InOut;

            case EzEaseType.BounceIn:       return EzEaseBounce.In;
            case EzEaseType.BounceOut:      return EzEaseBounce.Out;
            case EzEaseType.BounceInOut:    return EzEaseBounce.InOut;

            case EzEaseType.ElasticIn:      return EzEaseElastic.In;
            case EzEaseType.ElasticOut:     return EzEaseElastic.Out;
            case EzEaseType.ElasticInOut:   return EzEaseElastic.InOut;
            case EzEaseType.Punch:          return EzEaseElastic.Punch;

            case EzEaseType.CircIn:         return EzEaseCircular.In;
            case EzEaseType.CircOut:        return EzEaseCircular.Out;
            case EzEaseType.CircInOut:      return EzEaseCircular.InOut;

            case EzEaseType.CubicIn:        return EzEaseCubic.In;
            case EzEaseType.CubicOut:       return EzEaseCubic.Out;
            case EzEaseType.CubicInOut:     return EzEaseCubic.InOut;

            case EzEaseType.ExpoIn:         return EzEaseExponential.In;
            case EzEaseType.ExpoOut:        return EzEaseExponential.Out;
            case EzEaseType.ExpoInOut:      return EzEaseExponential.InOut;

            case EzEaseType.QuadIn:         return EzEaseQuadratic.In;
            case EzEaseType.QuadOut:        return EzEaseQuadratic.Out;
            case EzEaseType.QuadInOut:      return EzEaseQuadratic.InOut;

            case EzEaseType.QuartIn:        return EzEaseQuartic.In;
            case EzEaseType.QuartOut:       return EzEaseQuartic.Out;
            case EzEaseType.QuartInOut:     return EzEaseQuartic.InOut;

            case EzEaseType.QuintIn:        return EzEaseQuintic.In;
            case EzEaseType.QuintOut:       return EzEaseQuintic.Out;
            case EzEaseType.QuintInOut:     return EzEaseQuintic.InOut;
        }

        return EzEaseLinear.None;
    }
}