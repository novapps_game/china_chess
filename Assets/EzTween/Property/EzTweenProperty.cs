using System;

public class EzTweenProperty<T, A> : TweenProperty where A : ITweenable<T> {

	public string Name { get; protected set; }

	protected A tweenable;
    protected Type type;
    protected Property<T> property;

	protected T originalEndValue;
	protected T startValue;
	protected T endValue;
	protected T diffValue;

	public EzTweenProperty(string name, T endValue, bool relative = false)
        : this(null, name, endValue, relative) {
	}

    public EzTweenProperty(Type type, string name, T endValue, bool relative = false) : base(relative) {
        this.type = type;
        Name = name;
        originalEndValue = endValue;
        tweenable = Activator.CreateInstance<A>();
    }

    public override string ToString() {
		return GetType().Name + "." + Name;
	}

	public override bool ValidateTarget(object target) {
        if (type != null) {
            property = (Property<T>)Activator.CreateInstance(type, Name, target.GetType());
        } else {
            property = new Property<T>(Name, target.GetType());
        }
		return property.Valid;
	}

	public override void Start() {
		endValue = originalEndValue;

		if (owner.fromMode) {
			startValue = endValue;
			endValue = property.Get(owner.target);
		} else {
			startValue = property.Get(owner.target);
		}

		if (relative && !owner.fromMode) {
			diffValue = endValue;
		} else {
			diffValue = tweenable.Diff(startValue, endValue);
		}
	}

	public void Restart(T newEndValue) {
		originalEndValue = newEndValue;
		Start();
	}

	public override void Tick(float elapsed) {
		var easedTime = easeFunction(elapsed, 0, 1, owner.duration);
		var result = tweenable.Lerp(startValue, diffValue, easedTime);
        property.Set(owner.target, result);
	}
}
