using UnityEngine;
using System.Collections;


public static class EzTweenExtensions {
    #region Transform extensions

    // to tweens

    public static EzTween PositionTo(this Transform self, float duration, Vector3 endValue, bool isRelative = false) {
        return EzTweenManager.To(self, duration, new EzTweenConfig().Position(endValue, isRelative));
    }

    public static EzTween LocalPositionTo(this Transform self, float duration, Vector3 endValue, bool isRelative = false) {
        return EzTweenManager.To(self, duration, new EzTweenConfig().LocalPosition(endValue, isRelative));
    }

    public static EzTween RotationTo(this Transform self, float duration, Vector3 endValue, bool isRelative = false) {
        return EzTweenManager.To(self, duration, new EzTweenConfig().Rotation(endValue, isRelative));
    }

    public static EzTween LocalRotationTo(this Transform self, float duration, Vector3 endValue, bool isRelative = false) {
        return EzTweenManager.To(self, duration, new EzTweenConfig().LocalRotation(endValue, isRelative));
    }

    public static EzTween ScaleTo(this Transform self, float duration, float endValue, bool isRelative = false) {
        return self.ScaleTo(duration, new Vector3(endValue, endValue, endValue), isRelative);
    }

    public static EzTween ScaleTo(this Transform self, float duration, Vector3 endValue, bool isRelative = false) {
        return EzTweenManager.To(self, duration, new EzTweenConfig().Scale(endValue, isRelative));
    }

    public static EzTween Shake(this Transform self, float duration, Vector3 shakeMagnitude, EzShakeType shakeType = EzShakeType.Position, int frameMod = 1, bool useLocalProperties = false) {
        return EzTweenManager.To(self, duration, new EzTweenConfig().Shake(shakeMagnitude, shakeType, frameMod, useLocalProperties));
    }


    // from tweens

    public static EzTween PositionFrom(this Transform self, float duration, Vector3 endValue, bool isRelative = false) {
        return EzTweenManager.From(self, duration, new EzTweenConfig().Position(endValue, isRelative));
    }

    public static EzTween LocalPositionFrom(this Transform self, float duration, Vector3 endValue, bool isRelative = false) {
        return EzTweenManager.From(self, duration, new EzTweenConfig().LocalPosition(endValue, isRelative));
    }

    public static EzTween RotationFrom(this Transform self, float duration, Vector3 endValue, bool isRelative = false) {
        return EzTweenManager.From(self, duration, new EzTweenConfig().Rotation(endValue, isRelative));
    }

    public static EzTween LocalRotationFrom(this Transform self, float duration, Vector3 endValue, bool isRelative = false) {
        return EzTweenManager.From(self, duration, new EzTweenConfig().LocalRotation(endValue, isRelative));
    }

    public static EzTween ScaleFrom(this Transform self, float duration, Vector3 endValue, bool isRelative = false) {
        return EzTweenManager.From(self, duration, new EzTweenConfig().Scale(endValue, isRelative));
    }

    #endregion


    #region Material extensions

    public static EzTween ColorTo(this Material self, float duration, Color endValue, string colorName = "_Color") {
        return EzTweenManager.To(self, duration, new EzTweenConfig().MaterialColor(endValue, colorName));
    }

    public static EzTween ColorFrom(this Material self, float duration, Color endValue, string colorName = "_Color") {
        return EzTweenManager.From(self, duration, new EzTweenConfig().MaterialColor(endValue, colorName));
    }

    #endregion

}
