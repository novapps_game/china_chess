using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class EzTweenCollectionConfig {
    public int id; // id for finding the Tween at a later time. multiple Tweens can have the same id
    public int loops = 1; // number of times to iterate. -1 will loop indefinitely
    public EzLoopType loopType = EzTweenManager.defaultLoopType;
    public EzUpdateType updateType = EzTweenManager.defaultUpdateType;

    public Action<EzTweenBase> onInit;
    public Action<EzTweenBase> onStart;
    public Action<EzTweenBase> onLoopBegin;
    public Action<EzTweenBase> onUpdate;
    public Action<EzTweenBase> onLoopEnd;
    public Action<EzTweenBase> onComplete;

    /// <summary>
    /// sets the number of iterations. setting to -1 will loop infinitely
    /// </summary>
    public EzTweenCollectionConfig SetLoops(int loops) {
        this.loops = loops;
        return this;
    }

    /// <summary>
    /// sets the number of iterations and the loop type. setting to -1 will loop infinitely
    /// </summary>
    public EzTweenCollectionConfig SetLoops(int loops, EzLoopType loopType) {
        this.loops = loops;
        this.loopType = loopType;
        return this;
    }

    /// <summary>
    /// sets the update type for the Tween
    /// </summary>
    public EzTweenCollectionConfig SetUpdateType(EzUpdateType updateType) {
        this.updateType = updateType;
        return this;
    }


    /// <summary>
    /// sets the onInit handler for the Tween
    /// </summary>
    public EzTweenCollectionConfig OnInit(Action<EzTweenBase> action) {
        onInit = action;
        return this;
    }


    /// <summary>
    /// sets the onBegin handler for the Tween
    /// </summary>
    public EzTweenCollectionConfig OnStart(Action<EzTweenBase> action) {
        onStart = action;
        return this;
    }


    /// <summary>
    /// sets the onIterationStart handler for the Tween
    /// </summary>
    public EzTweenCollectionConfig OnLoopBegin(Action<EzTweenBase> action) {
        onLoopBegin = action;
        return this;
    }


    /// <summary>
    /// sets the onUpdate handler for the Tween
    /// </summary>
    public EzTweenCollectionConfig OnUpdate(Action<EzTweenBase> action) {
        onUpdate = action;
        return this;
    }


    /// <summary>
    /// sets the onIterationEnd handler for the Tween
    /// </summary>
    public EzTweenCollectionConfig OnLoopEnd(Action<EzTweenBase> action) {
        onLoopEnd = action;
        return this;
    }


    /// <summary>
    /// sets the onComplete handler for the Tween
    /// </summary>
    public EzTweenCollectionConfig OnComplete(Action<EzTweenBase> action) {
        onComplete = action;
        return this;
    }

    /// <summary>
    /// sets the id for the Tween. Multiple Tweens can have the same id and you can retrieve them with the Go class
    /// </summary>
    public EzTweenCollectionConfig SetId(int id) {
        this.id = id;
        return this;
    }

}