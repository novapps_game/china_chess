using System;
using System.Collections.Generic;
using UnityEngine;


public class EzTweenConfig {
	private List<TweenProperty> tweenProperties = new List<TweenProperty>();

	public List<TweenProperty> TweenProperties { get { return tweenProperties; } }

	public int id;
	// id for finding the Tween at a later time. multiple Tweens can have the same id
	public float delay;
	// how long should we delay before starting the Tween
	public int loops = 1;
	// number of times to iterate. -1 will loop indefinitely
	public float timeScale = 1;
	public EzLoopType loopType = EzTweenManager.defaultLoopType;
	public EzEaseType easeType = EzTweenManager.defaultEaseType;
	public EzUpdateType updateType = EzTweenManager.defaultUpdateType;
	public bool pausedOnStart;
	public bool fromMode;

	public Action<EzTweenBase> onInit;
	public Action<EzTweenBase> onStart;
	public Action<EzTweenBase> onLoopBegin;
	public Action<EzTweenBase> onUpdate;
	public Action<EzTweenBase> onLoopEnd;
	public Action<EzTweenBase> onComplete;

	#region TweenProperty adders

	/// <summary>
	/// position tween
	/// </summary>
	public EzTweenConfig Position(Vector3 endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector3, Vector3Tweenable>("position", endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	/// <summary>
	/// localPosition tween
	/// </summary>
	public EzTweenConfig LocalPosition(Vector3 endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector3, Vector3Tweenable>("localPosition", endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	/// <summary>
	/// position path tween
	/// </summary>
	public EzTweenConfig PositionPath(EzSpline path, bool relative = false, EzLookAtType lookAtType = EzLookAtType.None, Transform lookTarget = null) {
		var prop = new EzTweenPositionPath(path, relative, false, lookAtType, lookTarget);
		tweenProperties.Add(prop);
		return this;
	}

	/// <summary>
	/// position path tween
	/// </summary>
	public EzTweenConfig LocalPositionPath(EzSpline path, bool relative = false, EzLookAtType lookAtType = EzLookAtType.None, Transform lookTarget = null) {
		var prop = new EzTweenPositionPath(path, relative, true, lookAtType, lookTarget);
		tweenProperties.Add(prop);
		return this;
	}

    /// <summary>
    /// position path tween
    /// </summary>
    public EzTweenConfig AnchorPositionPath(EzSpline path, bool relative = false, EzLookAtType lookAtType = EzLookAtType.None, RectTransform lookTarget = null) {
        var prop = new EzTweenAnchorPositionPath(path, relative, lookAtType, lookTarget);
        tweenProperties.Add(prop);
        return this;
    }

    /// <summary>
    /// uniform scale tween (x, y and z scale to the same value)
    /// </summary>
    public EzTweenConfig Scale(float endValue, bool relative = false) {
		return Scale(new Vector3(endValue, endValue, endValue), relative);
	}

	/// <summary>
	/// scale tween
	/// </summary>
	public EzTweenConfig Scale(Vector3 endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector3, Vector3Tweenable>("localScale", endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	/// <summary>
	/// scale through a series of Vector3s
	/// </summary>
	public EzTweenConfig ScalePath(EzSpline path, bool relative = false) {
		var prop = new EzTweenVector3Path("localScale", path, relative);
		tweenProperties.Add(prop);
		return this;
	}

	/// <summary>
	/// rotation tween
	/// </summary>
	public EzTweenConfig Rotation(Vector3 endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector3, Vector3Tweenable>("eulerAngles", endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	/// <summary>
	/// localRotation tween
	/// </summary>
	public EzTweenConfig LocalRotation(Vector3 endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector3, Vector3Tweenable>("localEulerAngles", endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	/// <summary>
	/// material color tween
	/// </summary>
	public EzTweenConfig MaterialColor(Color endValue, string colorName = "_Color", bool relative = false) {
		var prop = new EzTweenMaterialColor(colorName, endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	/// <summary>
	/// shake tween
	/// </summary>
	public EzTweenConfig Shake(Vector3 shakeMagnitude, EzShakeType shakeType = EzShakeType.Position, int frameMod = 1, bool localSpace = false) {
		var prop = new EzTweenShake(shakeMagnitude, shakeType, frameMod, localSpace);
		tweenProperties.Add(prop);
		return this;
	}

	#region generic properties

	public EzTweenConfig TweenInt(string propertyName, int endValue, bool relative = false) {
		var prop = new EzTweenProperty<int, IntTweenable>(propertyName, endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenFloat(string propertyName, float endValue, bool relative = false) {
		var prop = new EzTweenProperty<float, FloatTweenable>(propertyName, endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenVector2(string propertyName, Vector2 endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector2, Vector2Tweenable>(propertyName, endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenVector2X(string propertyName, float endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector2, Vector2Tweenable>(typeof(Vector2XProperty), propertyName, new Vector2(endValue, 0), relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenVector2Y(string propertyName, float endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector2, Vector2Tweenable>(typeof(Vector2YProperty), propertyName, new Vector2(0, endValue), relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenVector3(string propertyName, Vector3 endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector3, Vector3Tweenable>(propertyName, endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenVector3X(string propertyName, float endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector3, Vector3Tweenable>(typeof(Vector3XProperty), propertyName, new Vector3(endValue, 0, 0), relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenVector3Y(string propertyName, float endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector3, Vector3Tweenable>(typeof(Vector3YProperty), propertyName, new Vector3(0, endValue, 0), relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenVector3Z(string propertyName, float endValue, bool relative = false) {
		var prop = new EzTweenProperty<Vector3, Vector3Tweenable>(typeof(Vector3ZProperty), propertyName, new Vector3(0, 0, endValue), relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenVector3Path(string propertyName, EzSpline path, bool relative = false) {
		var prop = new EzTweenVector3Path(propertyName, path, relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenColor(string propertyName, Color endValue, bool relative = false) {
		var prop = new EzTweenProperty<Color, ColorTweenable>(propertyName, endValue, relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenColorR(string propertyName, float endValue, bool relative = false) {
		var prop = new EzTweenProperty<Color, ColorTweenable>(typeof(ColorRProperty), propertyName, new Color(endValue, 0, 0, 0), relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenColorG(string propertyName, float endValue, bool relative = false) {
		var prop = new EzTweenProperty<Color, ColorTweenable>(typeof(ColorGProperty), propertyName, new Color(0, endValue, 0, 0), relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenColorB(string propertyName, float endValue, bool relative = false) {
		var prop = new EzTweenProperty<Color, ColorTweenable>(typeof(ColorBProperty), propertyName, new Color(0, 0, endValue, 0), relative);
		tweenProperties.Add(prop);
		return this;
	}

	public EzTweenConfig TweenColorA(string propertyName, float endValue, bool relative = false) {
		var prop = new EzTweenProperty<Color, ColorTweenable>(typeof(ColorAProperty), propertyName, new Color(0, 0, 0, endValue), relative);
		tweenProperties.Add(prop);
		return this;
	}

	#endregion

	#endregion

	/// <summary>
	/// adds a TweenProperty to the list
	/// </summary>
	public EzTweenConfig AddTweenProperty(TweenProperty tweenProp) {
		tweenProperties.Add(tweenProp);
		return this;
	}


	/// <summary>
	/// clears out all the TweenProperties
	/// </summary>
	public EzTweenConfig ClearProperties() {
		tweenProperties.Clear();
		return this;
	}

	/// <summary>
	/// clears out all the TweenProperties
	/// </summary>
	public EzTweenConfig ClearEvents() {
		onInit = null;
		onStart = null;
		onLoopBegin = null;
		onUpdate = null;
		onLoopEnd = null;
		onComplete = null;
		return this;
	}

	/// <summary>
	/// sets the delay for the tween
	/// </summary>
	public EzTweenConfig SetDelay(float seconds) {
		delay = seconds;
		return this;
	}


	/// <summary>
	/// sets the number of iterations. setting to -1 will loop infinitely
	/// </summary>
	public EzTweenConfig SetLoops(int loops) {
		this.loops = loops;
		return this;
	}


	/// <summary>
	/// sets the number of iterations and the loop type. setting to -1 will loop infinitely
	/// </summary>
	public EzTweenConfig SetLoops(int loops, EzLoopType loopType) {
		this.loops = loops;
		this.loopType = loopType;
		return this;
	}


	/// <summary>
	/// sets the timeScale to be used by the Tween
	/// </summary>
	public EzTweenConfig SetTimeScale(float timeScale) {
		this.timeScale = timeScale;
		return this;
	}


	/// <summary>
	/// sets the ease type for the Tween
	/// </summary>
	public EzTweenConfig SetEaseType(EzEaseType easeType) {
		this.easeType = easeType;
		return this;
	}


	/// <summary>
	/// sets whether the Tween should start paused
	/// </summary>
	public EzTweenConfig SetPausedOnStart(bool pausedOnStart) {
		this.pausedOnStart = pausedOnStart;
		return this;
	}


	/// <summary>
	/// sets the update type for the Tween
	/// </summary>
	public EzTweenConfig SetUpdateType(EzUpdateType setUpdateType) {
		updateType = setUpdateType;
		return this;
	}


	/// <summary>
	/// sets if this Tween should be a "from" Tween. From Tweens use the current property as the endValue and
	/// the endValue as the start value
	/// </summary>
	public EzTweenConfig SetFromMode(bool fromMode) {
		this.fromMode = fromMode;
		return this;
	}

	/// <summary>
	/// sets the onInit handler for the Tween
	/// </summary>
	public EzTweenConfig OnInit(Action<EzTweenBase> action) {
		onInit = action;
		return this;
	}

	/// <summary>
	/// sets the onBegin handler for the Tween
	/// </summary>
	public EzTweenConfig OnStart(Action<EzTweenBase> action) {
		onStart = action;
		return this;
	}

	/// <summary>
	/// sets the onIterationStart handler for the Tween
	/// </summary>
	public EzTweenConfig OnLoopBegin(Action<EzTweenBase> action) {
		onLoopBegin = action;
		return this;
	}


	/// <summary>
	/// sets the onUpdate handler for the Tween
	/// </summary>
	public EzTweenConfig OnUpdate(Action<EzTweenBase> action) {
		onUpdate = action;
		return this;
	}


	/// <summary>
	/// sets the onIterationEnd handler for the Tween
	/// </summary>
	public EzTweenConfig OnLoopEnd(Action<EzTweenBase> action) {
		onLoopEnd = action;
		return this;
	}


	/// <summary>
	/// sets the onComplete handler for the Tween
	/// </summary>
	public EzTweenConfig OnComplete(Action<EzTweenBase> action) {
		onComplete = action;
		return this;
	}


	/// <summary>
	/// sets the id for the Tween. Multiple Tweens can have the same id and you can retrieve them with the Go class
	/// </summary>
	public EzTweenConfig SetId(int id) {
		this.id = id;
		return this;
	}

}
