﻿using UnityEngine;
using System.Collections;

public class TweenRotate : Tween {

    public Vector3 rotation;
    public bool localSpace = true;

    public override void PlayNow() {
        iTween.RotateAdd(gameObject, iTween.Hash("amount", rotation,
            "time", time, "delay", delay, "islocal", localSpace,
            "easetype", easeType, "looptype", loopType,
            "onupdate", "OnUpdate", "oncomplete", "OnComplete", "oncompleteparams", this));
    }

    public override string Type() {
        return "rotate";
    }
}
