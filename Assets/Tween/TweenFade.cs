﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenFade : Tween {

    public float alpha = 0;
    public bool includeChildren = true;

    public override void PlayNow() {
        iTween.FadeTo(gameObject, iTween.Hash("alpha", alpha,
            "time", time, "delay", delay, "easetype", easeType, "looptype", loopType,
            "includechildren", includeChildren,
            "onupdate", "OnUpdate", "oncomplete", "OnComplete", "oncompleteparams", this));
    }

    public override string Type() {
        return "color";
    }
}
