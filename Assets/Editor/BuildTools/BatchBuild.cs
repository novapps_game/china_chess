﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;

public class BatchBuild {

    static string[] SCENES = FindEnabledEditorScenes();

    [MenuItem("BatchBuild/Android/All")]
    static void PerformAndroidAllBuild() {
        PerformAndroidGooglePlayBuild();
        PerformAndroid360Build();
        PerformAndroidBaiduBuild();
        PerformAndroidTencentBuild();
        PerformAndroidHuaweiBuild();
        PerformAndroidXiaomiBuild();
        PerformAndroidVivoBuild();
        PerformAndroidOppoBuild();
        PerformAndroidWandoujiaBuild();
        PerformAndroidAdeskBuild();
    }

    [MenuItem("BatchBuild/Android/Google Play")]
    static void PerformAndroidGooglePlayBuild() {
        BulidTarget("googleplay", "GOOGLE_PLAY", "Android");
    }

    [MenuItem("BatchBuild/Android/360")]
    static void PerformAndroid360Build() {
        BulidTarget("360", "_360", "Android");
    }

    [MenuItem("BatchBuild/Android/Baidu")]
    static void PerformAndroidBaiduBuild() {
        BulidTarget("baidu", "BAIDU", "Android");
    }

    [MenuItem("BatchBuild/Android/Tencent")]
    static void PerformAndroidTencentBuild() {
        BulidTarget("tencent", "TENCENT", "Android");
    }

    [MenuItem("BatchBuild/Android/Huawei")]
    static void PerformAndroidHuaweiBuild() {
        BulidTarget("huawei", "HUAWEI", "Android");
    }

    [MenuItem("BatchBuild/Android/Xiaomi")]
    static void PerformAndroidXiaomiBuild() {
        BulidTarget("xiaomi", "XIAOMI", "Android");
    }

    [MenuItem("BatchBuild/Android/Vivo")]
    static void PerformAndroidVivoBuild() {
        BulidTarget("vivo", "VIVO", "Android");
    }

    [MenuItem("BatchBuild/Android/Oppo")]
    static void PerformAndroidOppoBuild() {
        BulidTarget("oppo", "OPPO", "Android");
    }

    [MenuItem("BatchBuild/Android/Wandoujia")]
    static void PerformAndroidWandoujiaBuild() {
        BulidTarget("wandoujia", "WANDOUJIA", "Android");
    }

    [MenuItem("BatchBuild/Android/Adesk")]
    static void PerformAndroidAdeskBuild() {
        BulidTarget("adesk", "ADESK", "Android");
    }

    [MenuItem("BatchBuild/iOS/All")]
    static void PerformIOSAllBuild() {
        
    }

    static void BulidTarget(string name, string symbol, string target) {
        string projectPath = Application.dataPath.Replace("/Assets", "");
        string targetPath = projectPath + "/Build";
        string targetName = PlayerSettings.productName;

        BuildTarget buildTarget;
        BuildTargetGroup targetGroup;
        if (target == "Android") {
            targetPath = targetPath + "/Apk";
            targetName = PlayerSettings.productName + "_" + name + "_" + PlayerSettings.bundleVersion + ".apk";
            targetGroup = BuildTargetGroup.Android;
            buildTarget = BuildTarget.Android;
            if (Directory.Exists(targetPath)) {
                if (File.Exists(targetName)) {
                    File.Delete(targetName);
                }
            } else {
                Directory.CreateDirectory(targetPath);
            }
        } else if (target == "iOS") {
            targetPath = targetPath + "/iOSBuild";
            targetName = PlayerSettings.productName + "_" + name;
            targetGroup = BuildTargetGroup.iOS;
            buildTarget = BuildTarget.iOS;
        } else {
            Debug.LogError("Unsupported build target: " + target);
            return;
        }

        string originSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(targetGroup);
        string symbols = originSymbols;
        if (!symbols.Contains(symbol)) {
            symbols += ";" + symbol;
        }
        Debug.Log("Start building channel: " + name + " with symbols: " + symbols);
        PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, symbols);
        GenericBuild(SCENES, targetPath + "/" + targetName, buildTarget);
        PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, originSymbols);
        Debug.Log("Finish building channel: " + name + " and restore symbols to: " + 
            PlayerSettings.GetScriptingDefineSymbolsForGroup(targetGroup));
    }

    private static string[] FindEnabledEditorScenes() {
        List<string> EditorScenes = new List<string>();
        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
            if (!scene.enabled) continue;
            EditorScenes.Add(scene.path);
        }
        return EditorScenes.ToArray();
    }

    static void GenericBuild(string[] scenes, string targetDir, BuildTarget buildTarget, BuildOptions buildOptions = BuildOptions.None) {
        EditorUserBuildSettings.SwitchActiveBuildTarget(buildTarget);
        string result = BuildPipeline.BuildPlayer(scenes, targetDir, buildTarget, buildOptions);
        if (result.Length > 0) {
            throw new Exception("Build failure: " + result);
        }
    }
}
