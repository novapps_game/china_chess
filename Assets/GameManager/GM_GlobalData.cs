﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    public Color[] rarityColors;

    public int chessMode;//0:竞技    1:人机       2:双人

    public int[,] chess = new int[10, 9]{
        {2,3,6,5,1,5,6,3,2},
        {0,0,0,0,0,0,0,0,0},
        {0,4,0,0,0,0,0,4,0},
        {7,0,7,0,7,0,7,0,7},
        {0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0},
        {14,0,14,0,14,0,14,0,14},
        {0,11,0,0,0,0,0,11,0},
        {0,0,0,0,0,0,0,0,0},
        {9,10,13,12,8,12,13,10,9}
    };
    public bool isMoving = false;

}
