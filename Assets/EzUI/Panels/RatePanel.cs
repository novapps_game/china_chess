﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RatePanel : Panel {

	protected override void OnStart() {
		base.OnStart();
		transform.Find("InnerPanel/Title").GetComponent<Text>().text = EzRemoteConfig.rateTitle;
        transform.Find("InnerPanel/Buttons/Later").gameObject.SetActive(!EzRemoteConfig.rateForce);
    }

	protected override void OnClose() {
		base.OnClose();
		//GameManager.instance.OpenGameOver();
	}

	public void Rate() {
		OnClick("Rate");
		GameManager.instance.GotoRate();
		Close();
	}
}
