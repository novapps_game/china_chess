﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GotoUrlButton : MonoBehaviour {

    public string url;

	// Use this for initialization
	void Start() {
        GetComponent<Button>().onClick.AddListener(() => {
            if (!string.IsNullOrEmpty(url)) {
                Application.OpenURL(url);
            }
        });
	}
	
	// Update is called once per frame
	void Update() {
		
	}
}
