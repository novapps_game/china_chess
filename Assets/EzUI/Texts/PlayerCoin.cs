﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCoin : MonoBehaviour {

    public string format = "{0:D}";
    public string formatKey;

    public bool updatePerFrame = true;
    private IntText intText;
    private Text text;
    private ContentSizeFitter fitter;

    void Awake() {
        intText = GetComponent<IntText>();
        fitter = GetComponent<ContentSizeFitter>();
        if (intText != null) {
            intText.format = format;
            intText.formatKey = formatKey;
        } else {
            text = GetComponent<Text>();
            if (text != null) {
                if (!string.IsNullOrEmpty(formatKey)) {
                    format = Localization.GetMultilineText(formatKey);
                }
            }
        }
    }

    void OnEnable() {
        UpdateText();
    }

    // Update is called once per frame
    void Update() {
        if (updatePerFrame) {
            UpdateText();
        }
    }

    void UpdateText() {
        if (intText != null) {
            intText.targetValue = GameManager.instance.coins;
        } else if (text != null) {
            text.text = string.Format(format, GameManager.instance.coins);
        }
        if (fitter != null) {
            fitter.SetLayoutHorizontal();
        }
    }
}
