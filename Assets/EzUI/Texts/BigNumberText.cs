﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BigNumberText : MonoBehaviour {

    public double Value {
        get { return _value; }
        set {
            if (_value != value) {
                _value = value;
                UpdateText();
            }
        }
    }

    public Color color {
        get { return text.color; }
        set { text.color = value; }
    }

    public bool displayPositiveSymbol;
    public bool disableChineseDigits;

    private double _value;
    private Digit[] digits;
    private Text text;

    void Awake() {
        text = GetComponent<Text>();
        digits = Digit.DIGITS;
        UpdateText();
    }

    void UpdateText() {
        if (displayPositiveSymbol && _value > 0) {
            text.text = "+" + Digit.Format(_value, digits);
        } else {
            text.text = Digit.Format(_value, digits);
        }
    }
}
