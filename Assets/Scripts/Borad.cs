﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Borad : MonoBehaviour {

    public GameObject chessboard;

    void Awake() {

    }

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    private void OnEnable()
    {
        Chessposition();
    }

    public void Gridding(){
        int xx = -448, yy = 532;
        for (int i = 1; i <= 90; i++)
        {
            GameObject ite = GameManager.instance.Instantiate("item"); //(GameObject)Instantiate(Resources.Load("item"));//找到预设体
            ite.transform.SetParent(chessboard.transform,false);           //给预设体指定添加到什么地方
            GameObject tmp = GameObject.Find(ite.name);    //找到这个预设体的名字，给他做一些操作
            tmp.transform.localScale = Vector3.one;
            tmp.name = "item" + i.ToString();                                           //suoyou所有的深度 都是5
            tmp.transform.localPosition = new Vector3(xx, yy, 0);
            xx += 111;
            if (xx > 448)
            {
                yy -= 111;
                xx = -448;
            }
        }
    }

    public void CreatChessPieces(string sql, GameObject game, string name, int count,int chessID)
    {
        //引用prefab 生成象棋的棋子
        GameObject chessPieces = GameManager.instance.Instantiate(sql);
        chessPieces.transform.SetParent(game.transform);
        GameObject tmp = GameObject.Find(chessPieces.name);
        tmp.name = name + count.ToString();
        tmp.transform.localPosition = Vector3.zero;
        tmp.transform.localScale = Vector3.one;
        if (GameManager.instance.chessMode == 3)
        {
            if (chessID < 8 && chessID >= 1)
            {
                tmp.transform.localRotation = Quaternion.Euler(0, 0, 180f);
            }
        }
    }
    public void Chessposition()
    {
        //初始化其他对象
        GameManager.instance.chess = new int[10, 9]{
        {2,3,6,5,1,5,6,3,2},
        {0,0,0,0,0,0,0,0,0},
        {0,4,0,0,0,0,0,4,0},
        {7,0,7,0,7,0,7,0,7},
        {0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0},
        {14,0,14,0,14,0,14,0,14},
        {0,11,0,0,0,0,0,11,0},
        {0,0,0,0,0,0,0,0,0},
        {9,10,13,12,8,12,13,10,9}
    };
        ChessPiecesClick.ChessMove = true;
        ChessPiecesClick.str = "红方走";
        ChessPiecesClick.whoWin = 0;

        InitChess.Count = 0;//重置悔棋记录对象
        InitChess.pos = new InitChess.QIZI[400];
        Gridding();
        int count = 1;
        for (int i = 1; i <= 90; i++)
        {
            GameObject tmp = GameObject.Find("item" + i.ToString());
            int x = System.Convert.ToInt32(Mathf.Abs(tmp.transform.localPosition.x + 448) / 111);
            int y = System.Convert.ToInt32(Mathf.Abs((tmp.transform.localPosition.y -532) / 111));
            switch (GameManager.instance.chess[y, x])
            {
                case 1:
                    count++;
                    CreatChessPieces("b_jiang", tmp, "b_jiang", count,1);
                    break;
                case 2:
                    count++;
                    CreatChessPieces("b_ju", tmp, "b_ju", count,2);
                    break;
                case 3:
                    count++;
                    CreatChessPieces("b_ma", tmp, "b_ma", count,3);
                    break;
                case 4:
                    count++;
                    CreatChessPieces("b_pao", tmp, "b_pao", count,4);
                    break;
                case 5:
                    count++;
                    CreatChessPieces("b_shi", tmp, "b_shi", count,5);
                    break;
                case 6:
                    count++;
                    CreatChessPieces("b_xiang", tmp, "b_xiang", count,6);
                    break;
                case 7:
                    count++;
                    CreatChessPieces("b_zu", tmp, "b_zu", count,7);
                    break;
                case 8:
                    count++;
                    CreatChessPieces("r_shuai", tmp, "r_shuai", count,8);
                    break;
                case 9:
                    count++;
                    CreatChessPieces("r_ju", tmp, "r_ju", count,9);
                    break;
                case 10:
                    count++;
                    CreatChessPieces("r_ma", tmp, "r_ma", count,10);
                    break;
                case 11:
                    count++;
                    CreatChessPieces("r_pao", tmp, "r_pao", count,11);
                    break;
                case 12:
                    count++;
                    CreatChessPieces("r_shi", tmp, "r_shi", count,12);
                    break;
                case 13:
                    count++;
                    CreatChessPieces("r_xiang", tmp, "r_xiang", count,13);
                    break;
                case 14:
                    count++;
                    CreatChessPieces("r_bing", tmp, "r_bing", count,14);
                    break;
            }
        }
    }
}
