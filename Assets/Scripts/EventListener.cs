﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventListener
{

    public delegate void OnWhoWinDelegate(int newVal);
    public event OnWhoWinDelegate OnWhoWinChange;

    public int whoWinVal = 0;
    public int whoWinLen
    {
        get
        {
            return whoWinVal;
        }
        set
        {
            if (whoWinVal == value) return;
            if (OnWhoWinChange != null)
                OnWhoWinChange(whoWinVal);
            whoWinVal = value;
        }
    }

    public delegate void OnCanKillDelegate(bool newVal);
    public event OnCanKillDelegate OnCanKillChange;

    public bool canKillVal = false;
    public bool CanKillLen
    {
        get
        {
            return canKillVal;
        }
        set
        {
            if (canKillVal == value) return;
            if (OnCanKillChange != null)
                OnCanKillChange(!canKillVal);
            canKillVal = value;
        }
    }
}
