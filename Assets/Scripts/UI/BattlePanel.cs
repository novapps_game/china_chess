﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattlePanel : Panel {
    public Text playerName;
    public Text level;
    public Text coin;
    public Text score;
    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start()
    {

    }

    private void OnEnable()
    {
        score.text = GameManager.instance.score.ToString();
        playerName.text = GameManager.instance.playerName;
        level.text = GameController.GetLevel(GameManager.instance.score);
        isOpen = true;
    }

    // Update is called once per frame
    void Update()
    {
        coin.text = GameManager.instance.coins.ToString();
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    public void DummyLevel(int level)
    {
        SearchEngine.m_nSearchDepth = level;
        switch (level)
        {
            case 1:
                if (GameManager.instance.coins < 10)
                {
                    GameController.instance.warNingStr = "金币不足,\n无法入场,看视频即可获取10金币";
                    WarningPanel.level = 1;
                    Open("WarningPanel");
                    return;
                }
                GameManager.instance.CostCoins(10);
                break;
            case 2:
                if (GameManager.instance.score < 300)
                {
                    GameController.instance.warNingStr = "积分不足300，\n无法入场";
                    WarningPanel.level = 4;
                    Open("WarningPanel");
                    return;
                }
                if (GameManager.instance.coins < 50)
                {
                    GameController.instance.warNingStr = "金币不足,\n无法入场,看视频即可获取10金币";
                    WarningPanel.level = 2;
                    Open("WarningPanel");
                    return;
                }
                GameManager.instance.CostCoins(50);
                break;
            case 3:
                if (GameManager.instance.score < 1000)
                {
                    WarningPanel.level = 4;
                    GameController.instance.warNingStr = "积分不足1000，\n无法入场";
                    Open("WarningPanel");
                    return;
                }
                if (GameManager.instance.coins < 100)
                {
                    GameController.instance.warNingStr = "金币不足,\n无法入场,看视频即可获取10金币";
                    WarningPanel.level = 3;
                    Open("WarningPanel");
                    return;
                }
                GameManager.instance.CostCoins(100);
                break;
        }
        Open("SearchPanel");
        isClose = true;
    }

    public void SettingOnClick()
    {
        Open("SettingPanel");
    }

    public void ReturnOnClick()
    {
        Open("HomePanel");
        isClose = true;
    }
    public void ChangeData()
    {
        Open("ChangeDataPanel");
    }
}
