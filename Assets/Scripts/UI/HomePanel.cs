﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomePanel : Panel {

    public Text coin;
    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start () {

        if (EzPrefs.GetInt("第一次") != 300)
        {
            EzPrefs.SetInt("第一次", 300);
            GameManager.instance.UpdatePlayerName("江湖大侠");
            GameManager.instance.GainCoins(300);
        }
    }

    private void OnEnable()
    {
        //适配交叉推广
        GameManager.instance.OnHome();

        coin.text = GameManager.instance.coins.ToString();
        isOpen = true;
    }

    // Update is called once per frame
    void Update () {
        coin.text = GameManager.instance.coins.ToString();
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    public void SettingOnClick()
    {
        Open("SettingPanel");
    }

    public void AdOnClick()
    {
        Open("BuyPanel");
        isClose = true;
    }

    public void GameModeChoice(int mode)
    {
        switch (mode)
        {
            case 1:
                //竞技
                GameManager.instance.chessMode = 1;
                Open("BattlePanel");
                break;
            case 2:
                //人机
                GameManager.instance.chessMode = 2;
                Open("LevelCheckPanel");
                break;
            case 3:
                //双人
                GameManager.instance.chessMode = 3;
                Open("GamePanel");
                break;
        }
        isClose = true;
    }
}
