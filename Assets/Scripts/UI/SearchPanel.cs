﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SearchPanel : Panel {
    public Image selfHead;
    public Text selfName;
    public Text selfLevel;

    public Image otherHead;
    public Text otherName;
    public Text otherLevel;

    public GameObject tital;
    public GameObject otherBox;
    public GameObject vs;
    public GameObject ing;

    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;
    // Use this for initialization
    void Start()
    {

    }

    private void OnEnable()
    {


        otherBox.transform.GetComponent<Image>().color = ColorExtension.NewA(otherBox.transform.GetComponent<Image>().color, 0);
        foreach (Transform child in otherBox.transform)
        {
            if (child.name == "Head")
            {
                child.transform.GetComponent<Image>().color = ColorExtension.NewA(child.transform.GetComponent<Image>().color, 0);
            }
            else {
                child.transform.GetComponent<Text>().color = ColorExtension.NewA(child.transform.GetComponent<Text>().color, 0);
            }
        }

        vs.transform.GetComponent<Image>().color = ColorExtension.NewA(vs.transform.GetComponent<Image>().color, 0);
        tital.transform.GetComponent<Image>().color = ColorExtension.NewA(tital.transform.GetComponent<Image>().color, 0);
        ing.transform.GetComponent<Image>().color = ColorExtension.NewA(ing.transform.GetComponent<Image>().color, 1);

        RandomOther(otherName, otherLevel);
        selfName.text = GameManager.instance.playerName;
        selfLevel.text = GameController.GetLevel(GameManager.instance.score);


        isOpen = true;

        StartCoroutine("Wait");
    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    IEnumerator Wait()
    {
        int i = Random.Range(0, 10);
        yield return new WaitForSeconds(i);

        ing.GetComponent<TweenFade>().Play();
        vs.GetComponent<TweenFade>().Play();
        yield return new WaitForSeconds(1);
        otherBox.GetComponent<TweenFade>().Play();
        yield return new WaitForSeconds(1);
        tital.GetComponent<TweenFade>().Play();
        yield return new WaitForSeconds(2);
        Open("BattleGamePanel");
        isClose = true;
    }

    public void RandomOther(Text name,Text level)
    {
        int n = Random.Range(0, LoadTexts().Length);
        int i = Random.Range(-50, 50);
        GameController.instance.otherScore = GameManager.instance.score + i;
        if (GameController.instance.otherScore <= -30)
        {
            GameController.instance.otherScore = -30;
        }
        level.text = GameController.GetLevel(GameManager.instance.score + i);
        GameController.instance.otherLevel = GameController.GetLevel(GameManager.instance.score + i);
        name.text = LoadTexts()[n];
        GameController.instance.otherName = LoadTexts()[n];
    }

    string[] LoadTexts()
    {
        string[] nicknames = { };
        TextAsset ta = Resources.Load<TextAsset>("Texts/nicknames_cn");
        if (ta != null)
        {
            nicknames = ta.text.Split('\n');
        }
        return nicknames;
    }
}
