﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePanel : Panel {

    public Text coin;
    public Text level;
    public GameObject turn;
    public Text timeNum;
    public GameObject retractButton;
    public Sprite defaltSp;
    public Sprite doubleHumenRetractSp;

    public CanvasGroup cg;
    private bool isOpen = false;
    public bool isClose = false;

    private int second = 0, minute = 0;

    // Use this for initialization
    void Start(){

    }

    private void OnEnable()
    {
        EzAds.ShowBanner();
        retractButton.transform.GetComponent<Image>().sprite = defaltSp;
        coin.text = GameManager.instance.coins.ToString();
        isOpen = true;
        StartCoroutine("TimeStar");
        switch (SearchEngine.m_nSearchDepth)
        {
            case 1:
                level.text = "初级";
                break;
            case 2:
                level.text = "中级";
                break;
            case 3:
                level.text = "高级";
                break;
        }
        if (GameManager.instance.chessMode == 3)
        {
            level.text = "双人";
            retractButton.transform.GetComponent<Image>().sprite = doubleHumenRetractSp;
        }
    }

    // Update is called once per frame
    void Update () {
        coin.text = GameManager.instance.coins.ToString();
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                EzAds.HideBanner();
                Close();
            }
        }

        if (InitChess.Count == 0)
        {
            retractButton.transform.GetComponent<Button>().enabled = false;
            retractButton.transform.GetComponent<Image>().color = Color.gray;
        }
        else {
            retractButton.transform.GetComponent<Button>().enabled = true;
            retractButton.transform.GetComponent<Image>().color = Color.white;
        }


        if (ChessPiecesClick.ChessMove)
        {
            if (GameManager.instance.chessMode == 3)
            {
                turn.transform.localRotation = Quaternion.Euler(0, 0, 0);
            }
            turn.transform.GetComponent<Text>().color = Color.red;
            turn.transform.GetComponent<Text>().text = "红方走";
        }
        else if (!ChessPiecesClick.ChessMove)
        {
            if (GameManager.instance.chessMode == 3)
            {
                turn.transform.localRotation = Quaternion.Euler(0, 0, 180f);
            }
            turn.transform.GetComponent<Text>().color = Color.black;
            turn.transform.GetComponent<Text>().text = "黑方走";
        }
    }

    IEnumerator TimeStar()
    {
        while (true)
        {
            if (minute > 0)
            {
                timeNum.text = minute.ToString() + ":" + second.ToString();
            }
            else
            {
                timeNum.text = second.ToString();
            }

            yield return new WaitForSeconds(1.0f);
            second++;
            if (second == 60)
            {
                minute++;
                second = 0;
            }
        }
    }

    public void SettingOnClick()
    {
        Open("SettingPanel");
    }

    public void ReturnOnClick()
    {
        ExitPanel.typeText = "退出将丢失游戏进度";
        Open("ExitPanel");
    }

    public void Exit()
    {
        RestarGame();
        if (GameManager.instance.chessMode == 3)
        {
            Open("HomePanel");
        }
        else {
            Open("LevelCheckPanel");
        }
        isClose = true;
    }

    public void RestarOnClick()
    {
        Open("RestarPanel");
    }

    public void RetractOnClick()
    {
        if (GameManager.instance.chessMode == 3)
        {
            GameManager.instance.PlaySound("huiqi");
            InitChess initChess = new InitChess();
            initChess.IloveHUIQI();
            return;
        }

        if (GameManager.instance.coins < 10)
        {
            GameController.instance.warNingStr = "金币不足，看视频即可获取10金币";
            WarningPanel.level = 0;
            Open("WarningPanel");
            return;
        }
        else {
            GameManager.instance.CostCoins(10);
            GameManager.instance.PlaySound("huiqi");
            InitChess initChess = new InitChess();
            initChess.IloveHUIQI();
        }
    }

    public void RestarGame()
    {
        InitChess initChess = new InitChess();

        GameObject objA = GameObject.Find("chessAI");
        objA.transform.SetParent(GameObject.Find("Chessboard").transform);
        objA.transform.localPosition = new Vector3(8888, 8888, 0);

        GameObject objC = GameObject.Find("chessCheck");
        objC.transform.SetParent(GameObject.Find("Chessboard").transform);
        objC.transform.localPosition = new Vector3(8888, 8888, 0);

        ChessPiecesClick.whoWin = 0;
        GameManager.instance.isMoving = false;
        second = 0;
        minute = 0;
        for (int c = InitChess.Count; c > 1; c--)
        {
            initChess.IloveHUIQI(true);
        }
        for (int i = 1; i <= 90; i++)
        {
            GameObject game = GameObject.Find("item" + i.ToString());
            GameObject Clear = GameObject.Find("prefabs" + i.ToString());
            Destroy(game);
            Destroy(Clear);
        }
    }

    public void ChongXinKaiShi()
    {
        InitChess initChess = new InitChess();

        ChessPiecesClick.whoWin = 0;
        GameManager.instance.isMoving = false;
        second = 0;
        minute = 0;

        GameObject obj = GameObject.Find("chessAI");
        obj.transform.localPosition = new Vector3(8888, 8888, 0);

        for (int i = 1; i <= 90; i++)
        {
            GameObject Clear = GameObject.Find("prefabs" + i.ToString());
            Destroy(Clear);
        }
        if (GameManager.instance.chessMode == 1)
            for (int i = InitChess.Count; i > 1; i--)
                initChess.IloveHUIQI();
        else
            for (int i = InitChess.Count; i > 0; i--)
                initChess.IloveHUIQI(true);
    }
}
