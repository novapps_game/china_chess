﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitPanel : Panel {

    public Text text;
    public static string typeText;

    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start()
    {

    }

    private void OnEnable()
    {
        text.text = typeText;
        isOpen = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    public void Continue()
    {
        isClose = true;
    }

    public void ExitOnClick()
    {
        isClose = true;
        GameObject gg;
        switch (GameManager.instance.chessMode)
        {
            case 1:
                BattleGamePanel.go = false;
                gg = GameObject.Find("BattleGamePanel");
                gg.transform.GetComponent<BattleGamePanel>().Exit();
                break;
            case 2:
                gg = GameObject.Find("GamePanel");
                gg.transform.GetComponent<GamePanel>().Exit();
                break;
            case 3:
                gg = GameObject.Find("GamePanel");
                gg.transform.GetComponent<GamePanel>().Exit();
                break;
        }
    }
}
