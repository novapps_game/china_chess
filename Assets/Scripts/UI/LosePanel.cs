﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LosePanel : Panel {
    public Image chessBoardShot;
    public RectTransform chessBoardRect;
    public Text loseText;
    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start () {

    }

    private void OnEnable()
    {
        if (GameManager.instance.chessMode == 1)
        {
            GameObject gg = GameObject.Find("BattleGamePanel");
            gg.transform.GetComponent<BattleGamePanel>().RestarGame();
            gg.transform.GetComponent<BattleGamePanel>().isClose = true;
        }
        else {
            GameObject gg = GameObject.Find("GamePanel");
            gg.transform.GetComponent<GamePanel>().RestarGame();
            gg.transform.GetComponent<GamePanel>().isClose = true;
        }


        chessBoardShot.sprite = ReadImag.Action(Application.persistentDataPath + Path.DirectorySeparatorChar, "chessBoardShot.png");
        if (GameManager.instance.chessMode == 1)
        {
            switch (SearchEngine.m_nSearchDepth)
            {
                case 1:
                    GameManager.instance.CostCoins(10);
                    GameManager.instance.UpdateScore(GameManager.instance.score -= 10);
                    loseText.text = " -10 积分       -10 金币\n当前积分" + GameManager.instance.score.ToString();
                    break;
                case 2:
                    GameManager.instance.CostCoins(200);
                    GameManager.instance.UpdateScore(GameManager.instance.score -= 25);
                    loseText.text = " -25 积分       -200 金币\n当前积分" + GameManager.instance.score.ToString();
                    break;
                case 3:
                    GameManager.instance.CostCoins(400);
                    GameManager.instance.UpdateScore(GameManager.instance.score -= 95);
                    loseText.text = " -95 积分       -400 金币\n当前积分：" + GameManager.instance.score.ToString();
                    break;
            }
            if (BattleGamePanel.isOverTime)
            {
                loseText.text += "\n超时判负";
                BattleGamePanel.isOverTime = false;
            }
        }
        else {
            loseText.text = "江东父老多才俊\n卷土重来未可知";
        }
        isOpen = true;
    }

    // Update is called once per frame
    void Update () {
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
                GameManager.instance.PlaySound("shibai");
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    public void Restar()
    {
        switch (GameManager.instance.chessMode)
        {
            case 1:
                Open("SearchPanel");
                break;
            case 2:
                Open("GamePanel");
                break;
            case 3:
                Open("GamePanel");
                break;
        }
        isClose = true;
    }

    public void Return()
    {
        Open("HomePanel");
        isClose = true;
    }

}
