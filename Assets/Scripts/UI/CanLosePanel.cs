﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanLosePanel : Panel {
    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start()
    {

    }
    private void OnEnable()
    {
        isOpen = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }


    public void No()
    {
        isClose = true;
    }

    public void Yes()
    {
        BattleGamePanel.go = false;
        GameObject gg = GameObject.Find("BattleGamePanel");
        gg.transform.GetComponent<BattleGamePanel>().RestarGame();
        gg.transform.GetComponent<BattleGamePanel>().isClose = true;
        Open("LosePanel");
        isClose = true;
    }
}
