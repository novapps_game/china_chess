﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCheckPanel : Panel {

    public Text coin;
    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start () {

    }

    private void OnEnable()
    {
        isOpen = true;
    }

    // Update is called once per frame
    void Update () {
        coin.text = GameManager.instance.coins.ToString();
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    public void ComputerLevel(int level)
    {
        SearchEngine.m_nSearchDepth = level;
        Open("GamePanel");
        isClose = true;
    }

    public void SettingOnClick()
    {
        Open("SettingPanel");
    }

    public void ReturnOnClick()
    {
        Open("HomePanel");
        isClose = true;
    }
}
