﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class BattleGamePanel : Panel {

    public Text selfName;
    public Text selfLevel;
    public Text selfTime;
    public Text selfScore;

    public Text otherName;
    public Text otherLevel;
    public Text otherTime;
    public Text otherScore;

    public GameObject bTurn;
    public GameObject rTurn;

    public CanvasGroup cg;
    private bool isOpen = false;
    public bool isClose = false;

    public static int rStepTimes = 0;
    public static int bStepTimes = 0;
    public static int totalTime = 31;

    public static bool isOverTime = false;
    public static bool go = true;
    // Use this for initialization
    void Start()
    {

    }

    private void OnEnable()
    {
        go = true;
        EzAds.ShowBanner();
        selfName.text = GameManager.instance.playerName;
        selfScore.text = GameManager.instance.score.ToString();
        selfLevel.text = GameController.GetLevel(GameManager.instance.score);
        otherName.text = GameController.instance.otherName;
        otherLevel.text = GameController.instance.otherLevel;
        otherScore.text = "积分：" +  GameController.instance.otherScore.ToString();
        isOpen = true;
        StartCoroutine("TimeStar");
    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                EzAds.HideBanner();
                Close();
            }
        }
        if (!ChessPiecesClick.ChessMove)
        {
            bTurn.SetActive(true);
            rTurn.SetActive(false);
        }
        else if (ChessPiecesClick.ChessMove)
        {
            bTurn.SetActive(false);
            rTurn.SetActive(true);
        }
    }

    IEnumerator TimeStar()
    {
        while (go)
        {
            if (totalTime > 0)
            {
                if(ChessPiecesClick.ChessMove)
                {
                    selfTime.text = "步时：00:" + totalTime.ToString();
                }
                else if (!ChessPiecesClick.ChessMove)
                {
                    otherTime.text = "步时：00:" + totalTime.ToString();
                }
            }
            else
            {
                //超时判负
                GameObject overTime = GameObject.Find("OverTime");
                overTime.transform.GetComponent<TweenFade>().Play();
                yield return new WaitForSeconds(2.0f);
                go = false;
                isOverTime = true;
                Open("LosePanel");
            }

            yield return new WaitForSeconds(1.0f);
            totalTime--;
        }
    }

    public void SettingOnClick()
    {
        Open("SettingPanel");
    }

    public void LoseOnClick()
    {
        rTurn.SetActive(false);
        RectTransform chessBoardRect = GameObject.Find("BChessboard").transform.GetComponent<RectTransform>();
        StartCoroutine(EzScreenshot.Capture(Application.persistentDataPath + Path.DirectorySeparatorChar + "chessBoardShot.png", chessBoardRect, (x) => {
            Open("CanLosePanel");
        }));
    }

    public void ReturnOnClick()
    {
        rTurn.SetActive(false);
        ExitPanel.typeText = "退出等于认输";
        RectTransform chessBoardRect = GameObject.Find("BChessboard").transform.GetComponent<RectTransform>();
        StartCoroutine(EzScreenshot.Capture(Application.persistentDataPath + Path.DirectorySeparatorChar + "chessBoardShot.png", chessBoardRect, (x) => {
            Open("ExitPanel");
        }));
    }

    public void Exit()
    {
        Open("LosePanel");
    }

    public void RestarGame()
    {
        InitChess initChess = new InitChess();

        GameObject objA = GameObject.Find("BChessAI");
        objA.transform.SetParent(GameObject.Find("BChessboard").transform);
        objA.transform.localPosition = new Vector3(8888, 8888, 0);

        GameObject objC = GameObject.Find("BChessCheck");
        objC.transform.SetParent(GameObject.Find("BChessboard").transform);
        objC.transform.localPosition = new Vector3(8888, 8888, 0);

        ChessPiecesClick.whoWin = 0;
        GameManager.instance.isMoving = false;
        totalTime = 30;
        for (int c = InitChess.Count; c > 1; c--)
        {
            initChess.IloveHUIQI(true);
        }
        for (int i = 1; i <= 90; i++)
        {
            GameObject game = GameObject.Find("item" + i.ToString());
            GameObject Clear = GameObject.Find("prefabs" + i.ToString());
            Destroy(game);
            Destroy(Clear);
        }
    }
}
