﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPanel : Panel {

    public Toggle musicToggle;
    public Toggle soundToggle;
    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start () {
        soundToggle.onValueChanged.AddListener(SoundOnValueChanged);
        musicToggle.onValueChanged.AddListener(MusicOnValueChanged);
    }

    private void OnEnable()
    {
        isOpen = true;
    }

    // Update is called once per frame
    void Update () {
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    public void SoundOnValueChanged(bool isOn)
    {
        if (isOn)
        {
            GameManager.instance.muteSound = false;
        }
        else {
            GameManager.instance.muteSound = true;
        }
    }

    public void MusicOnValueChanged(bool isOn)
    {
        if (isOn)
        {
            GameManager.instance.muteMusic = false;
        }
        else
        {
            GameManager.instance.muteMusic = true;
        }
    }

    public void RetractOnClick()
    {
        isClose = true;
    }
}
