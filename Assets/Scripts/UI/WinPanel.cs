﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class WinPanel : Panel {
    public Image chessBoardShot;
    public GameObject doubleCoin;
    public Text getCoin;
    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start()
    {

    }

    private void OnEnable()
    {


        GameObject gg = GameObject.Find("GamePanel");
        gg.transform.GetComponent<GamePanel>().RestarGame();
        gg.transform.GetComponent<GamePanel>().isClose = true;

        chessBoardShot.sprite = ReadImag.Action(Application.persistentDataPath + Path.DirectorySeparatorChar, "chessBoardShot.png");

        isOpen = true;

        if (GameManager.instance.chessMode == 3)
        {
            doubleCoin.SetActive(false);
            getCoin.text = "再接再厉！";
            return;
        }
        else
        {
            doubleCoin.SetActive(true);
        }

        switch (SearchEngine.m_nSearchDepth)
        {
            case 1:
                getCoin.text = "+" + 30 + " 金币";
                GameManager.instance.GainCoins(30);
                break;
            case 2:
                getCoin.text = "+" + 40 + " 金币";
                GameManager.instance.GainCoins(40);
                break;
            case 3:
                getCoin.text = "+" + 50 + " 金币";
                GameManager.instance.GainCoins(50);
                break;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
                GameManager.instance.PlaySound("shengli");
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    public void Restar()
    {
        Open("GamePanel");
        isClose = true;
    }

    public void Return()
    {
        Open("HomePanel");
        isClose = true;
    }

    public void DoubleCoin()
    {
        doubleCoin.SetActive(false);
        if (EzAds.IsRewardedVideoReady())
        {
            EzAds.ShowRewardedVideo(() => {
                switch (SearchEngine.m_nSearchDepth)
                {
                    case 1:
                        getCoin.text = "+" + 60 + " 金币";
                        GameManager.instance.GainCoins(30);
                        break;
                    case 2:
                        getCoin.text = "+" + 80 + " 金币";
                        GameManager.instance.GainCoins(40);
                        break;
                    case 3:
                        getCoin.text = "+" + 100 + " 金币";
                        GameManager.instance.GainCoins(50);
                        break;
                }
            });
        }
        else
        {
            Toast.Show("广告尚未加载。\n请稍后......");
        }
    }
}
