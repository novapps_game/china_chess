﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class BattleWinPanel : Panel {
    public Image chessBoardShot;
    public GameObject doubleCoin;
    public Text score;
    public Text getCoin;
    public Text getScore;
    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start()
    {

    }

    private void OnEnable()
    {
        doubleCoin.SetActive(true);
        GameObject gg = GameObject.Find("BattleGamePanel");
        gg.transform.GetComponent<BattleGamePanel>().RestarGame();
        gg.transform.GetComponent<BattleGamePanel>().isClose = true;

        chessBoardShot.sprite = ReadImag.Action(Application.persistentDataPath + Path.DirectorySeparatorChar, "chessBoardShot.png");
        switch (SearchEngine.m_nSearchDepth)
        {
            case 1:
                GameManager.instance.GainCoins(50);
                GameManager.instance.GainScore(10);
                getCoin.text = "+50 金币";
                getScore.text = "+10 积分";
                break;
            case 2:
                GameManager.instance.GainCoins(200);
                GameManager.instance.GainScore(30);
                getCoin.text = "+200 金币";
                getScore.text = "+30 积分";
                break;
            case 3:
                GameManager.instance.GainCoins(400);
                GameManager.instance.GainScore(100);
                getCoin.text = "+400 金币";
                getScore.text = "+100 积分";
                break;
        }
        isOpen = true;
    }

    // Update is called once per frame
    void Update()
    {
        score.text = "当前积分：" + GameManager.instance.score.ToString();
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
                GameManager.instance.PlaySound("shengli");
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    public void Restar()
    {
        BattleGamePanel.go = true;
        Open("BattleGamePanel");
        isClose = true;
    }

    public void Return()
    {
        Open("HomePanel");
        isClose = true;
    }

    public void DoubleCoin()
    {
        doubleCoin.SetActive(false);
        if (EzAds.IsRewardedVideoReady())
        {
            EzAds.ShowRewardedVideo(() => {
                switch (SearchEngine.m_nSearchDepth)
                {
                    case 1:
                        GameManager.instance.GainCoins(50);
                        getCoin.text = "+100 金币";
                        break;
                    case 2:
                        GameManager.instance.GainCoins(200);
                        getCoin.text = "+400 金币";
                        break;
                    case 3:
                        GameManager.instance.GainCoins(400);
                        getCoin.text = "+800 金币";
                        break;
                }
            });
        }
        else
        {
            Toast.Show("广告尚未加载。\n请稍后......");
        }
    }

}
