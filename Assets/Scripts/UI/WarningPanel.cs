﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningPanel : Panel {

    public CanvasGroup cg;
    public GameObject returnButton;
    public GameObject adsButton;
    public static int level;
    public Text warning;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start()
    {

    }

    private void OnEnable()
    {
        if (level == 4)
        {
            returnButton.SetActive(false);
            adsButton.SetActive(false);
            EzTiming.CallDelayed(2f, () => {
                isClose = true;
            });
        }
        else {
            returnButton.SetActive(true);
            adsButton.SetActive(true);
        }
        warning.text = GameController.instance.warNingStr;
        isOpen = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    public void Return()
    {
        isClose = true;
    }

    public void Ads()
    {
        switch (level)
        {
            case 0:
                if (EzAds.IsRewardedVideoReady())
                {
                    EzAds.ShowRewardedVideo(() => {
                        GameManager.instance.CostCoins(10);
                        GameManager.instance.PlaySound("huiqi");
                        InitChess initChess = new InitChess();
                        initChess.IloveHUIQI();
                    });
                }
                else
                {
                    Toast.Show("广告尚未加载。\n请稍后......");
                }
                isClose = true;
                break;
            case 1:
            case 2:
            case 3:
                if (EzAds.IsRewardedVideoReady())
                {
                    EzAds.ShowRewardedVideo(() => {
                        SearchEngine.m_nSearchDepth = level;
                        Open("SearchPanel");
                        isClose = true;
                    });
                }
                else
                {
                    Toast.Show("广告尚未加载。\n请稍后......");
                }
                break;
            case 4:
                isClose = true;
                break;
        }
    }
}
