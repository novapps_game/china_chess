﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyPanel : Panel {

    public CanvasGroup cg ;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }    
    private void OnEnable()
    {
        isOpen = true;
    }

    public void ReturnOnClick()
    {
        Open("LevelCheckPanel");
        isClose = true;
    }
}
