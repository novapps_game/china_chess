﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeDataPanel : Panel {
    public InputField inputName;
    public CanvasGroup cg;
    private bool isOpen = false;
    private bool isClose = false;

    // Use this for initialization
    void Start()
    {

    }

    private void OnEnable()
    {
        isOpen = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            cg.alpha += Time.deltaTime * 4f;
            if (cg.alpha >= 1)
            {
                isOpen = false;
            }
        }
        if (isClose)
        {
            cg.alpha -= Time.deltaTime * 4f;
            if (cg.alpha <= 0)
            {
                isClose = false;
                Close();
            }
        }
    }

    public void Exit()
    {
        isClose = true;
    }

    public void Ok()
    {
        GameManager.instance.UpdatePlayerName(inputName.text);
        GameObject bpObj = GameObject.Find("BattlePanel");
        bpObj.transform.GetComponent<BattlePanel>().playerName.text = GameManager.instance.playerName;
        isClose = true;
    }
}
