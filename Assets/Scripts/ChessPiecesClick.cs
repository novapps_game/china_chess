﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ChessPiecesClick : MonoBehaviour {

    public static string str = "";
    public static int FromX = -1, ToX = -1, ToY = -1, FromY = -1;
    public static int whoWin = 0;//判断是否有人赢了  //0:无   1:黑  2:红
    public static string RedName = null, BlackName = null, ItemName;//blackchessname  and   redchessname
    public static bool ChessMove = true;//true   redMove   false BlackMove
    public GameObject ai;//识别人机走到哪里
    public GameObject check;//标识当前选中的棋子
    public GameObject tips;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public int IsBlackOrRed(GameObject obj)
    {
        if (obj.name.Substring(0, 1) == "r")
            return 2;
        else if (obj.name.Substring(0, 1) == "b")
            return 1;
        else
            return 0;
    }

    public void BlackNameOrRedName(GameObject obj)
    {//得到棋子的名字
        if (obj.name.Substring(0, 1) == "r")
            RedName = obj.name;//得到red名字
        else if (obj.name.Substring(0, 1) == "b")
            BlackName = obj.name;//得到black名字
        else
            ItemName = obj.name;//得到item名字
    }

    //吃子类
    public void IsEat(string Frist, string sconde, int x1, int y1, int x2, int y2)
    {
        GameObject Onename = GameObject.Find(Frist);//得到第一个
        GameObject Twoname = GameObject.Find(sconde);//得到第二个名字
        GameObject Twofather = Twoname.gameObject.transform.parent.gameObject;//得到第二个的父亲
        Onename.gameObject.transform.SetParent(Twofather.transform);
        TweenMove tm = Onename.GetComponent<TweenMove>();
        tm.position = Vector3.zero;
        tm.Play();
        GameManager.instance.isMoving = true;
        BattleGamePanel.totalTime = 31;
        EzTiming.CallDelayed(1f,()=> {
            GameManager.instance.chess[y2, x2] = GameManager.instance.chess[y1, x1];
            GameManager.instance.chess[y1, x1] = 0;
            GameObject eatC = GameObject.Find("eatChess");
            switch (GameManager.instance.chessMode)
            {
                case 1:
                    eatC = GameObject.Find("BEatChess");
                    break;
                case 2:
                    eatC = GameObject.Find("eatChess");
                    break;
                case 3:
                    eatC = GameObject.Find("eatChess");
                    break;
            }
            Twoname.transform.parent = eatC.transform;
            Twoname.transform.localPosition = new Vector3(5000, 5000, 0);
            GameManager.instance.isMoving = false;
            Onename.transform.localScale = Vector3.one;

            GameManager.instance.PlaySound("chi");
        });
    }

    //移动类
    public void IsMove(string One, GameObject game, int x1, int y1, int x2, int y2)
    {
        GameObject qi = GameObject.Find(One);
        qi.transform.SetParent(game.transform);
        TweenMove tm = qi.GetComponent<TweenMove>();
        tm.position = Vector3.zero;
        tm.Play();
        BattleGamePanel.totalTime = 31;
        GameManager.instance.isMoving = true;
        EzTiming.CallDelayed(1f, () => {
            GameManager.instance.chess[y2, x2] = GameManager.instance.chess[y1, x1];
            GameManager.instance.chess[y1, x1] = 0;
            GameManager.instance.isMoving = false;
            qi.transform.localScale = Vector3.one;
            GameManager.instance.PlaySound("luozi");
        });
    }

    InitChess initC = new InitChess();
    MoveReminder moveReminder = new MoveReminder();
    SearchEngine search = new SearchEngine();

    public void IsClickCheck(GameObject obj)
    {
        if (GameManager.instance.isMoving)
            return;
        switch (GameManager.instance.chessMode)
        {
            case 1:
                ai = GameObject.Find("BChessAI");
                check = GameObject.Find("BChessCheck");
                tips = GameObject.Find("BTips");
                break;
            case 2:
                ai = GameObject.Find("chessAI");
                check = GameObject.Find("chessCheck");
                tips = GameObject.Find("Tips");
                break;
            case 3:
                ai = GameObject.Find("chessAI");
                check = GameObject.Find("chessCheck");
                tips = GameObject.Find("Tips");
                break;
        }
        if (whoWin != 0)
            return;
        string lastRName = RedName;
        string lastBName = BlackName;
        BlackNameOrRedName(obj);//是否点击到棋子  如果是  就得到棋子
        int Result = IsBlackOrRed(obj);//判断点击到了什么
        if (obj.name.Substring(0, 1) != "i"){
            obj = obj.gameObject.transform.parent.gameObject;//得到他的父容器
        }
        int x = System.Convert.ToInt32(Mathf.Abs(obj.transform.localPosition.x + 448) / 111);
        int y = System.Convert.ToInt32(Mathf.Abs((obj.transform.localPosition.y - 532) / 111));


        switch (Result)
        {
            case 0://点击到了空  是否要走棋
                   //如果点击到了空格  就把对象清空
                check.transform.localPosition = new Vector3(8888, 8888, 0);
                for (int i = 1; i <= 90; i++)
                {
                    GameObject Clear = GameObject.Find("prefabs" + i.ToString());
                    Destroy(Clear);
                }
                ToY = y;
                ToX = x;
                if (ChessMove)
                {//红色走
                    if (RedName == null)
                        return;
                    string sssRed = RedName;//记录红色棋子的名字
                    bool ba = rules.IsValidMove(GameManager.instance.chess, FromX, FromY, ToX, ToY);
                    if (!ba)
                    {
                        GameObject rName = GameObject.Find(lastRName);
                        rName.transform.localScale = Vector3.one;
                        tips.GetComponent<TweenFade>().Play();
                        return;
                    }
                    int from = GameManager.instance.chess[FromY, FromX];
                    int to = GameManager.instance.chess[ToY, ToX];
                    initC.AddChess(InitChess.Count, FromX, FromY, ToX, ToY, true, from, to);
                    IsMove(RedName, obj, FromX, FromY, ToX, ToY);//走了

                    ChessMove = false;
                    EzTiming.CallDelayed(1f, () => {
                        str = "黑方走";
                        KingPosition.JiangJunCheck();
                        if (str == "红色棋子胜利")
                        {
                            return;//因为没有携程关系  每次进入黑色走棋的时候都判断 棋局是否结束
                        }
                        if (GameManager.instance.chessMode == 3)
                        {//如果现在是双人对战模式
                            BlackName = null;
                            RedName = null;
                            return;
                        }
                        if (ChessMove == false)
                            StartCoroutine("Robot");
                        //执行走棋
                    });
                }
                else
                {//黑色走
                    if (BlackName == null)
                        return;
                    bool ba = rules.IsValidMove(GameManager.instance.chess, FromX, FromY, ToX, ToY);
                    if (!ba)
                    {
                        GameObject bName = GameObject.Find(lastBName);
                        bName.transform.localScale = Vector3.one;
                        tips.GetComponent<TweenFade>().Play();
                        return;
                    }
                    int from = GameManager.instance.chess[FromY, FromX];
                    int to = GameManager.instance.chess[ToY, ToX];
                    initC.AddChess(InitChess.Count, FromX, FromY, ToX, ToY, true, from, to);
                    IsMove(BlackName, obj, FromX, FromY, ToX, ToY);
                    //黑色走棋
                    ChessMove = true;
                    EzTiming.CallDelayed(1f, () => {
                        KingPosition.JiangJunCheck();
                    });
                }
                break;
            case 1://点击到了黑色  是否选中  还是  红色要吃子
                if (!ChessMove)
                {
                    if (lastBName != null)
                    {
                        GameObject bName = GameObject.Find(lastBName);
                        bName.transform.localScale = Vector3.one;
                    }
                    transform.GetComponent<TweenScale>().Play();
                    check.transform.SetParent(transform);
                    check.transform.localPosition = Vector3.zero;
                    FromX = x;
                    FromY = y;

                    for (int i = 1; i <= 90; i++)
                    {
                        GameObject Clear = GameObject.Find("prefabs" + i.ToString());
                        Destroy(Clear);
                    }

                    moveReminder.ClickChess(FromX, FromY);
                }
                else
                {
                    check.transform.localPosition = new Vector3(8888, 8888, 0);
                    for (int i = 1; i <= 90; i++)
                    {
                        GameObject Clear = GameObject.Find("prefabs" + i.ToString());
                        Destroy(Clear);
                    }

                    if (RedName == null)
                        return;
                    ToX = x;
                    ToY = y;
                    bool ba = rules.IsValidMove(GameManager.instance.chess, FromX, FromY, ToX, ToY);
                    if (!ba)
                    {
                        tips.GetComponent<TweenFade>().Play();
                        return;
                    }
                    int from = GameManager.instance.chess[FromY, FromX];
                    int to = GameManager.instance.chess[ToY, ToX];
                    initC.AddChess(InitChess.Count, FromX, FromY, ToX, ToY, true, from, to);
                    //看看是否能播放音乐
                    IsEat(RedName, BlackName, FromX, FromY, ToX, ToY);
                    ChessMove = false;
                    EzTiming.CallDelayed(1f, () => {
                        //红色吃子  变黑色走
                        str = "黑方走";
                        KingPosition.JiangJunCheck();
                        if (str == "红色棋子胜利")
                        {
                            return;//因为没有携程关系  每次进入黑色走棋的时候都判断 棋局是否结束
                        }
                        if (GameManager.instance.chessMode == 3)
                        {
                            RedName = null;
                            BlackName = null;
                            return;
                        }
                        if (ChessMove == false)
                            StartCoroutine("Robot");
                    });
                }
                break;
            case 2://点击到了红色   是否选中  还是黑色要吃子
                if (ChessMove)
                {
                    if (lastRName != null)
                    {
                        GameObject rName = GameObject.Find(lastRName);
                        rName.transform.localScale = Vector3.one;
                    }
                    transform.GetComponent<TweenScale>().Play();
                    check.transform.SetParent(transform);
                    check.transform.localPosition = Vector3.zero;
                    FromX = x;
                    FromY = y;

                    for (int i = 1; i <= 90; i++)
                    {
                        GameObject Clear = GameObject.Find("prefabs" + i.ToString());
                        Destroy(Clear);
                    }

                    moveReminder.ClickChess(FromX, FromY);
                }
                else
                {
                    check.transform.localPosition = new Vector3 (8888,8888,0);
                    for (int i = 1; i <= 90; i++)
                    {
                        GameObject Clear = GameObject.Find("prefabs" + i.ToString());
                        Destroy(Clear);
                    }

                    if (BlackName == null)
                        return;
                    ToX = x;
                    ToY = y;
                    bool ba = rules.IsValidMove(GameManager.instance.chess, FromX, FromY, ToX, ToY);
                    if (!ba)
                    {
                        tips.GetComponent<TweenFade>().Play();
                        return;
                    }
                    int from = GameManager.instance.chess[FromY, FromX];
                    int to = GameManager.instance.chess[ToY, ToX];
                    initC.AddChess(InitChess.Count, FromX, FromY, ToX, ToY, true, from, to);
                    //看看是否能播放音乐
                    IsEat(BlackName, RedName, FromX, FromY, ToX, ToY);
                    ChessMove = true;
                    EzTiming.CallDelayed(1f, () => {
                        RedName = null;
                        BlackName = null;
                        str = "红方走";
                        KingPosition.JiangJunCheck();
                    });
                }
                break;
        }
    }

    IEnumerator Robot()
    {
        if (SearchEngine.m_nSearchDepth < 3)
        {
            int i = Random.Range(2, 5);
            yield return new WaitForSeconds(i);
        }
        else
        {
            yield return null;
        }

        Loom.RunAsync(() =>{
                Thread thread = new Thread(Threm);
                thread.Start();
            });
    }


    public void Threm()
    {
        //str="对方正在思考";
        if (ChessMove == false)
        {
            string s1 = "";
            string s2 = "";
            string s3 = "";
            string s4 = "";
            Blackmove.CHESSMOVE chess = search.SearchAGoodMove(GameManager.instance.chess);
            Loom.QueueOnMainThread((param) =>
            {
                s1 = search.Itemfirname(chess);
                s2 = search.Itemsconname(chess);
                GameObject one = GameObject.Find(s1);
                GameObject two = GameObject.Find(s2);
                foreach (Transform child in one.transform)
                    s3 = child.name;//第一个象棋名字
                foreach (Transform child in two.transform)
                    s4 = child.name;//吃到的子的象棋名字
                if (s4 == "")
                {
                    int from = GameManager.instance.chess[chess.From.y, chess.From.x];
                    int to = GameManager.instance.chess[chess.To.y, chess.To.x];
                    initC.AddChess(InitChess.Count, chess.From.x, chess.From.y, chess.To.x, chess.To.y, false, from, to);
                    IsMove(s3, two, chess.From.x, chess.From.y, chess.To.x, chess.To.y);
                    ai.transform.localPosition = one.transform.localPosition;
                }
                else
                {
                    int from = GameManager.instance.chess[chess.From.y, chess.From.x];
                    int to = GameManager.instance.chess[chess.To.y, chess.To.x];
                    initC.AddChess(InitChess.Count, chess.From.x, chess.From.y, chess.To.x, chess.To.y, false, from, to);
                    IsEat(s3, s4, chess.From.x, chess.From.y, chess.To.x, chess.To.y);
                    ai.transform.localPosition = one.transform.localPosition;
                }
                ChessMove = true;
                EzTiming.CallDelayed(1f, () => {
                    RedName = null;
                    BlackName = null;
                    str = "红方走";
                    KingPosition.JiangJunCheck();
                });
            }, null);           
        }
    }
}
