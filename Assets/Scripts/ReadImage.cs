﻿using UnityEngine;
using System.IO;

public class ReadImag{

    public static Sprite Action(string path, string name)
    {
        Sprite tempSprite = new Sprite();
        Texture2D m_Tex = new Texture2D(1, 1);
        m_Tex.LoadImage(ReadPNG(path + name));
        tempSprite = Sprite.Create(m_Tex, new Rect(0, 0, m_Tex.width, m_Tex.height), new Vector2(0, 0));
        return tempSprite;
    }

    private static byte[] ReadPNG(string path)
    {
        FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
        fileStream.Seek(0, SeekOrigin.Begin);
        byte[] binary = new byte[fileStream.Length]; //创建文件长度的buffer
        fileStream.Read(binary, 0, (int)fileStream.Length);
        fileStream.Close();
        fileStream.Dispose();
        fileStream = null;
        return binary;
    }
}
