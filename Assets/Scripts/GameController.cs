﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GameController : Singleton<GameController> {

    private EventListener listen = new EventListener();

    private RectTransform chessBoardRect;
    private GameObject killTips;
    public static bool canKill = false;
    public string otherName;
    public string otherLevel;
    public int otherScore;


    public string warNingStr = "";

    // Use this for initialization
    void Start() {
        GameManager.instance.OnGameStart();
        listen.OnWhoWinChange += WinChange;
        GameManager.instance.PlayMusic();
    }

    // Update is called once per frame
    void Update() {
        Panel.CheckBackButton(() => GameManager.instance.LoadScene("Home"));
        listen.whoWinLen = ChessPiecesClick.whoWin;
        if (canKill)
        {
            CanKillChange();
        }
        if (GameManager.instance.score <= -30)
        {
            GameManager.instance.UpdateScore(-30);
        }
        if (otherScore <= -30)
        {
            otherScore = -30;
        }
    }

    public void Revive() {
        // Todo: revive player
    }

    public void GameOver() {
        GameManager.instance.OnGameOver();
    }

    public void WinChange(int i) {
        switch (GameManager.instance.chessMode)
        {
            case 1:
                chessBoardRect = GameObject.Find("BChessboard").transform.GetComponent<RectTransform>();
                break;
            case 2:
                chessBoardRect = GameObject.Find("Chessboard").transform.GetComponent<RectTransform>();
                break;
            case 3:
                chessBoardRect = GameObject.Find("Chessboard").transform.GetComponent<RectTransform>();
                break;
        }
        if (ChessPiecesClick.whoWin == 0) {
            return; 
        } else if (ChessPiecesClick.whoWin == 1)
        {
            GameManager.instance.GameOver();

            StartCoroutine(EzScreenshot.Capture(Application.persistentDataPath + Path.DirectorySeparatorChar + "chessBoardShot.png", chessBoardRect,(x) => {
                if (GameManager.instance.chessMode == 3)
                {
                    Panel.Open("WinPanel");
                }
                else {
                    Panel.Open("LosePanel");
                }
            }));
        }
        else if (ChessPiecesClick.whoWin == 2)
        {
            GameManager.instance.GameOver();

            StartCoroutine(EzScreenshot.Capture(Application.persistentDataPath + Path.DirectorySeparatorChar + "chessBoardShot.png", chessBoardRect,(x)=> {
                switch (GameManager.instance.chessMode)
                {
                    case 1:
                        Panel.Open("BattleWinPanel");
                        break;
                    case 2:
                        Panel.Open("WinPanel");
                        break;
                    case 3:
                        Panel.Open("WinPanel");
                        break;
                }
            }));
        }
    }

    public void CanKillChange()
    {
        if (ChessPiecesClick.whoWin != 0)
        {
            canKill = false;
            return;
        }
        switch (GameManager.instance.chessMode)
        {
            case 1:
                killTips = GameObject.Find("BKillTips");
                break;
            case 2:
                killTips = GameObject.Find("KillTips");
                break;
            case 3:
                killTips = GameObject.Find("KillTips");
                break;
        }
        killTips.GetComponent<TweenFade>().Play();
        GameManager.instance.PlaySound("jiangjun");
        canKill = false;
    }

    //获取等级
    public static string GetLevel(int score)
    {
        string level = "";
        if (score>= -30 && score<10)
        {
            level = "业余一级";
        }
        else if (score >= 10 && score < 30)
        {
            level = "业余二级";
        }
        else if (score >= 30 && score < 60)
        {
            level = "业余三级";
        }
        else if (score >= 60 && score < 100)
        {
            level = "业余四级";
        }
        else if (score >= 100 && score < 150)
        {
            level = "业余五级";
        }
        else if (score >= 150 && score < 210)
        {
            level = "业余六级";
        }
        else if (score >= 210 && score < 280)
        {
            level = "一级棋手";
        }
        else if (score >= 280 && score < 400)
        {
            level = "二级棋手";
        }
        else if (score >= 400 && score < 500)
        {
            level = "三级棋手";
        }
        else if (score >= 500 && score < 650)
        {
            level = "四级棋手";
        }
        else if (score >= 650 && score < 800)
        {
            level = "五级棋手";
        }
        else if (score >= 800 && score < 1000)
        {
            level = "六级棋手";
        }
        else if (score >= 1000 && score < 2000)
        {
            level = "七级棋手";
        }
        else if (score >= 2000 && score < 3000)
        {
            level = "八级棋手";
        }
        else if (score >= 3000 && score < 5000)
        {
            level = "九级棋手";
        }
        else if (score >= 5000 && score < 10000)
        {
            level = "地方大师";
        }
        else if (score >= 10000 && score < 20000)
        {
            level = "国家大师";
        }
        else if (score >= 20000)
        {
            level = "特级大师";
        }
        return level;
    }

    public void GetGift()
    {
        if (EzAds.IsRewardedVideoReady())
        {
            EzAds.ShowRewardedVideo(() => {
                GameManager.instance.GainCoins(10);
                warNingStr = "恭喜获得10金币！";
                WarningPanel.level = 4;
                Panel.Open("WarningPanel");
            });
        }
        else
        {
            Toast.Show("广告尚未加载。\n请稍后......");
        }
    }
}
