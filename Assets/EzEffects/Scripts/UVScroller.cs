﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVScroller : MonoBehaviour {

    public float horizontalSpeed;
    public float verticalSpeed;

    private Renderer mRenderer;

    // Use this for initialization
    void Start() {
        mRenderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update() {
        mRenderer.material.mainTextureOffset = new Vector2(
            Time.time * horizontalSpeed,
            Time.time * verticalSpeed);
    }
}
