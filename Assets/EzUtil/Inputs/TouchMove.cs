﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMove : MonoBehaviour {

    public float touchSlop = 0.01f;
    public float moveSpeed = 6f;
    public Rect limitRect;

    public enum Mode {
        Horizontal,
        Vertical,
        Both
    }
    public Mode mode;

    private Vector3 touchPos;

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        } else if (Input.GetMouseButton(0)) {
            Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 offset = newPos - touchPos;
            switch (mode) {
                case Mode.Horizontal:
                    if (Mathf.Abs(offset.x) > touchSlop) {
                        transform.position = transform.position.OffsetX(offset.x).Clamp(limitRect.ToBounds());
                    }
                    break;
                case Mode.Vertical:
                    if (Mathf.Abs(offset.y) > touchSlop) {
                        transform.position = transform.position.OffsetX(offset.x).Clamp(limitRect.ToBounds());
                    }
                    break;
                case Mode.Both:
                    if (offset.magnitude > touchSlop) {
                        transform.position = (transform.position + offset).Clamp(limitRect.ToBounds());
                    }
                    break;
            }
        }
        touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}
