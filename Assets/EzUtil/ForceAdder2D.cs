﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ForceAdder2D : MonoBehaviour {

    public class ForceParam {
        public Vector2 force;
        public ForceMode2D mode;

        public override string ToString() {
            return JsonUtility.ToJson(this);
        }

        public static ForceParam FromJson(string json) {
            return JsonUtility.FromJson<ForceParam>(json);
        }
    }

    private Rigidbody2D body2D;

    private void Awake() {
        body2D = GetComponent<Rigidbody2D>();
        //ForceParam param = new ForceParam();
        //param.force = new Vector2(10, 5);
        //param.mode = ForceMode2D.Force;
        //Debug.Log("ForceParam: " + param);
    }

    // AddForce: {"force":{"x":10.0,"y":5.0},"mode":0}
    // AddImpulse: {"force":{"x":10.0,"y":5.0},"mode":1}
    public void AddForce(string force) {
        ForceParam param = ForceParam.FromJson(force);
        body2D.AddForce(param.force, param.mode);
    }
}
