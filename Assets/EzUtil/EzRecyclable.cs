﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzRecyclable : MonoBehaviour {

	public virtual void Recycle() {
        CancelInvoke();
        gameObject.SetActive(false);
    }
}
