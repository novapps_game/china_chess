﻿using UnityEngine;
using System.Runtime.InteropServices;

public class EzClipboard {
    static IClipboard _clipboard;
    static IClipboard clipboard {
        get {
            if (_clipboard == null) {
#if UNITY_EDITOR
                _clipboard = new EditorClipboard();
#elif UNITY_ANDROID
                _clipboard = new AndroidClipboard();
#elif UNITY_IOS
                _clipboard = new IOSClipboard();
#endif
            }
            return _clipboard;
        }
    }

    public static void SetText(string text) {
        try {
            if (!string.IsNullOrEmpty(text)) {
                clipboard.SetText(text);
            }
        } catch (System.Exception ex) {
            Debug.LogWarning("Failed to set text to clipboard: " + ex.Message);
        }
    }

    public static string GetText() {
        string text = "";
        try {
            text = clipboard.GetText();
        } catch (System.Exception ex) {
            Debug.LogWarning("Failed to get text from clipboard: " + ex.Message);
        }
        return text;
    }
}

interface IClipboard {
    void SetText(string str);
    string GetText();
}

class EditorClipboard : IClipboard {
    public void SetText(string text) {
        GUIUtility.systemCopyBuffer = text;
    }

    public string GetText() {
        return GUIUtility.systemCopyBuffer;
    }
}

#if UNITY_IOS
class IOSClipboard : IClipboard {
    [DllImport("__Internal")]
    static extern void setTextToClipboard(string text);
    [DllImport("__Internal")]
    static extern string getTextFromClipboard();

    public void SetText(string text) {
        if (Application.platform != RuntimePlatform.OSXEditor) {
            setTextToClipboard(text);
        }
    }

    public string GetText() {
        return getTextFromClipboard();
    }
}
#endif

#if UNITY_ANDROID
class AndroidClipboard : IClipboard {
    public void SetText(string text) {
        AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass contextClass = new AndroidJavaClass("android.content.Context");
        AndroidJavaObject clipboardManager = activity.Call<AndroidJavaObject>("getSystemService", contextClass.GetStatic<string>("CLIPBOARD_SERVICE"));
        AndroidJavaClass clipDataClass = new AndroidJavaClass("android.content.ClipData");
        AndroidJavaObject clipData = clipDataClass.CallStatic<AndroidJavaObject>("newPlainText", "data", text);
        clipboardManager.Call("setPrimaryClip", clipData);
    }

    public string GetText() {
        AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass contextClass = new AndroidJavaClass("android.content.Context");
        AndroidJavaObject clipboardManager = activity.Call<AndroidJavaObject>("getSystemService", contextClass.GetStatic<string>("CLIPBOARD_SERVICE"));
        if (!clipboardManager.Call<bool>("hasPrimaryClip")) return "";
        AndroidJavaObject clipDescription = clipboardManager.Call<AndroidJavaObject>("getPrimaryClipDescription");
        AndroidJavaClass clipDescClass = new AndroidJavaClass("android.content.ClipDescription");
        if (!clipDescription.Call<bool>("hasMimeType", clipDescClass.GetStatic<string>("MIMETYPE_TEXT_PLAIN"))) return "";
        AndroidJavaObject clipData = clipboardManager.Call<AndroidJavaObject>("getPrimaryClip");
        AndroidJavaObject item = clipData.Call<AndroidJavaObject>("getItemAt", 0);
        AndroidJavaObject text = item.Call<AndroidJavaObject>("getText");
        return text.Call<string>("toString");
    }
}
#endif
