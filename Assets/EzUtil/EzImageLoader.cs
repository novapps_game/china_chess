﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class EzImageLoader {

    public const string IMAGE_CACHE = "ImageCache";
    public static readonly string imageCacheDir = Application.persistentDataPath + Path.DirectorySeparatorChar + IMAGE_CACHE;

    public static void Init() {
        if (!Directory.Exists(imageCacheDir)) {
            Directory.CreateDirectory(imageCacheDir);
        }
    }

    public static string GetCacheFilePath(string url) {
        return imageCacheDir + Path.DirectorySeparatorChar + url.GetHashCode();
    }

    public static string GetCachedFile(string url) {
        if (string.IsNullOrEmpty(url)) {
            return null;
        }
        string file = GetCacheFilePath(url);
        if (!File.Exists(file)) {
            return null;
        }
        return file;
    }

    public static bool IsCached(string url) {
        if (string.IsNullOrEmpty(url)) {
            return false;
        }
        string file = GetCacheFilePath(url);
        return File.Exists(file);
    }

    public static IEnumerator DownloadImage(string url,
        System.Action<Texture2D> onSuccess,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        if (!string.IsNullOrEmpty(url)) {
            Debug.Log("downloading image: " + url);
            WWW www = new WWW(url);
            while (!www.isDone) {
                if (onProgress != null) {
                    onProgress.Invoke(www.progress);
                }
                yield return null;
            }
            if (!string.IsNullOrEmpty(www.error)) {
                Debug.LogWarning("download failed: " + url);
                if (onError != null) {
                    onError.Invoke(www.error);
                }
            } else {
                Debug.Log("downloaded texture: " + www.texture);
                if (www.texture != null && www.texture.width > 0 && www.texture.height > 0 && onSuccess != null) {
                    onSuccess.Invoke(www.texture);
                }
            }
        }
    }

    public static IEnumerator CacheImage(string url,
        System.Action<string> onSuccess = null,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        if (!string.IsNullOrEmpty(url)) {
            string file = GetCacheFilePath(url);
            if (!File.Exists(file)) {
                yield return DownloadPNG(url, file, onSuccess, onError, onProgress);
            } else {
                Debug.Log(url + " image already cached in: " + file);
                if (onProgress != null) {
                    onProgress.Invoke(1);
                }
                if (onSuccess != null) {
                    onSuccess.Invoke(file);
                }
            }
        }
    }

    public static IEnumerator CacheImages(string[] urls,
        System.Action<int> onComplete = null,
        System.Action<float> onProgress = null) {
        if (urls != null && urls.Length > 0) {
            int cachedCount = 0;
            for (int i = 0; i < urls.Length; ++i) {
                yield return CacheImage(urls[i], (file) => {
                    ++cachedCount;
                    if (onProgress != null) {
                        onProgress.Invoke((i + 1) / urls.Length);
                    }
                }, (error) => {
                }, (progress) => {
                    if (onProgress != null) {
                        onProgress.Invoke((i + progress) / urls.Length);
                    }
                });
            }
            if (onComplete != null) {
                onComplete.Invoke(cachedCount);
            }
        }
    }

    public static IEnumerator DownloadJPG(string url, string file,
        System.Action<string> onSuccess = null,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        yield return DownloadImage(url, (texture) => {
            Debug.Log("jpg downloaded: " + url + " ==> " + file);
            texture.SaveToJPG(file);
            if (onSuccess != null) {
                onSuccess.Invoke(file);
            }
        }, onError, onProgress);
    }

    public static IEnumerator DownloadPNG(string url, string file,
        System.Action<string> onSuccess = null,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        yield return DownloadImage(url, (texture) => {
            Debug.Log("png downloaded: " + url + " ==> " + file);
            texture.SaveToPNG(file);
            if (onSuccess != null) {
                onSuccess.Invoke(file);
            }
        }, onError, onProgress);
    }

    public static Texture2D LoadTexture(string file) {
        if (!File.Exists(file)) return null;
        byte[] data = File.ReadAllBytes(file);
        Texture2D texture = new Texture2D(2, 2);
        texture.LoadImage(data);
        return texture;
    }
    /*
    public static IEnumerator LoadTexture(string url, 
        System.Action<Texture2D> onSuccess,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        string file = GetCachedFile(url);
		Texture2D texture = LoadTexture(file);
		if (texture != null) {
			if (onSuccess != null) {
				onSuccess.Invoke(texture);
			}
			yield return null;
		} else {
            yield return DownloadImage(url, (tex) => {
                Debug.Log("cache image to: " + file);
				tex.SaveToPNG(file);
                if (onSuccess != null) {
					onSuccess.Invoke(tex);
                }
            }, onError, onProgress);
        }
    }
	*/
    public static IEnumerator LoadTexture(string url,
        System.Action<Texture2D> onSuccess,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        string file = GetCacheFilePath(url);
        if (!File.Exists(file)) {
            yield return DownloadImage(url, (texture) => {
                Debug.Log("cache image to: " + file);
                texture.SaveToPNG(file);
                if (onSuccess != null) {
                    onSuccess.Invoke(texture);
                }
            }, onError, onProgress);
        } else {
            url = (new System.Uri(file)).AbsoluteUri;
            yield return DownloadImage(url, onSuccess, onError, onProgress);
        }
    }

    public static IEnumerator LoadSprite(string url,
        System.Action<Sprite> onSuccess,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        yield return LoadSprite(url, new Vector2(0.5f, 0.5f), onSuccess, onError, onProgress);
    }

    public static IEnumerator LoadSprite(string url, Vector2 pivot,
        System.Action<Sprite> onSuccess,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        yield return LoadTexture(url, (texture) => {
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), pivot);
            if (onSuccess != null) {
                onSuccess.Invoke(sprite);
            }
        }, onError, onProgress);
    }

    public static IEnumerator LoadSprite(string url, Rect rect, Vector2 pivot,
        System.Action<Sprite> onSuccess,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        yield return LoadTexture(url, (texture) => {
            Sprite sprite = Sprite.Create(texture, rect, pivot);
            if (onSuccess != null) {
                onSuccess.Invoke(sprite);
            }
        }, onError, onProgress);
    }
}
