﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationHandler : BaseHandler {

    [SerializeField]
    private UnityEvent onAnimationStart = new UnityEvent();

    [SerializeField]
    private UnityEvent onAnimationEnd = new UnityEvent();

    [SerializeField]
    private UnityEvent onAnimationFrame = new UnityEvent();

    public void OnAnimationStart() {
        onAnimationStart.Invoke();
    }

    public void OnAnimationEnd() {
        onAnimationEnd.Invoke();
    }

    public void OnAnimationFrame() {
        onAnimationFrame.Invoke();
    }

}
