﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHandler : MonoBehaviour {

	public virtual void DestroySelf() {
        Destroy(this);
    }
}
